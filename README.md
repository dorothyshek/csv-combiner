## CSV Combiner

Write a command line program that takes several CSV files as arguments. Each CSV file (found in the fixtures directory of this repo) will have the same columns. Your script should output a new CSV file to stdout that contains the rows from each of the inputs along with an additional column that has the filename from which the row came (only the file's basename, not the entire path). Use filename as the header for the additional column.

---

## Input & Output

The files csv_combiner and test_csv_combiner were written in Python. csv_combiner.py will work for any specified separator, though the files used for testing are the delimiters ',' and '|'. Libraries used include pandas and argparse and the modules os and typing.

Examples: 
python ./csv_combiner.py --files ./fixtures/clothing.csv ./fixtures/accessories.csv > combined.csv
python ./csv_combiner.py --files ./fixtures/clothing_pipe.csv ./fixtures/accessories_pipe.csv --sep "|" > combined.csv

Note that the --sep argument is optional. The default value is ',' so the program will not work correctly on CSV files
with different separators (e.g. clothing_pipe.csv) unless the correct --sep argument is specified.