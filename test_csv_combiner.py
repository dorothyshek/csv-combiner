# -*- coding: utf-8 -*-
"""
Created on Sun Nov  7 15:59:12 2021

@author: Dorot
"""

import pytest
import csv_combiner
import pandas as pd
import argparse


parser = csv_combiner.create_parser()
args_pass_files_comma = parser.parse_args(['--files', './fixtures/clothing.csv', './fixtures/accessories.csv'])
args_pass_files_pipe = parser.parse_args(['--files', './fixtures/clothing_pipe.csv', './fixtures/accessories_pipe.csv', '--sep', '|'])
args_failed_files_empty_data = parser.parse_args(['--files', './fixtures/empty.csv'])
args_failed_files_not_found = parser.parse_args(['--files', './fixtures/does_not_exist.csv'])
args_failed_files_diff_column_names = parser.parse_args(['--files', './fixtures/accessories.csv', './fixtures/accessories_diff_columns.csv'])
args_failed_files_extra_column = parser.parse_args(['--files', './fixtures/accessories.csv', './fixtures/accessories_extra_column.csv'])


def test_files_empty_comma_valid():
    try:
        csv_combiner.check_empty(args_pass_files_comma)
    except (pd.errors.EmptyDataError):
        assert False, f'{args_pass_files_comma} raised an exception'        

def test_files_empty_pipe_valid():
    try:
        csv_combiner.check_empty(args_pass_files_pipe)
    except (pd.errors.EmptyDataError):
        assert False, f'{args_pass_files_pipe} raised an exception'        

def test_files_empty_invalid():
    with pytest.raises(pd.errors.EmptyDataError):
        csv_combiner.check_empty(args_failed_files_empty_data)

        
def test_files_exist_comma_valid():
    try:
        csv_combiner.check_empty(args_pass_files_comma)
    except FileNotFoundError:
        pytest.fail(f'{args_pass_files_comma} raised a FileNotFoundError exception')

def test_files_exist_pipe_valid():
    try:
        csv_combiner.check_empty(args_pass_files_pipe)
    except FileNotFoundError:
        pytest.fail(f'{args_pass_files_pipe} raised a FileNotFoundError exception')
        
def test_files_exist_invalid():
    with pytest.raises(FileNotFoundError):
        csv_combiner.check_empty(args_failed_files_not_found)
                
        
def test_column_names_comma_valid():
    try:
        csv_combiner.validate_files(args_pass_files_comma)
    except ValueError:
        pytest.fail(f'{args_pass_files_comma} raised a ValueError exception')

def test_column_names_pipe_valid():
    try:
        csv_combiner.validate_files(args_pass_files_pipe)
    except ValueError:
        pytest.fail(f'{args_pass_files_pipe} raised a ValueError exception')
    
def test_column_names_invalid():
    with pytest.raises(ValueError) as exc:
        csv_combiner.validate_files(args_failed_files_diff_column_names)
    assert 'The column names of two files are not the same.' in str(exc.value)


def test_num_columns_comma_valid():
    try:       
        csv_combiner.validate_files(args_pass_files_comma)
    except ValueError:        
        pytest.fail(f'{args_pass_files_comma} raised a ValueError exception')

def test_num_columns_pipe_valid():
    try:       
        csv_combiner.validate_files(args_pass_files_pipe)
    except ValueError:        
        pytest.fail(f'{args_pass_files_pipe} raised a ValueError exception')

def test_num_columns_invalid():
    with pytest.raises(ValueError) as exc:
        csv_combiner.validate_files(args_failed_files_extra_column)
    assert 'The number of columns between two files is not the same.' in str(exc.value)


def test_output_shape_valid():
    output = csv_combiner.concatenate_files(args_pass_files_comma)            
    file_names = args_pass_files_comma.files
    n_rows = output.shape[0]
    m_rows = 0
    for f in file_names:
        df = pd.read_csv(f)
        m_rows += df.shape[0]
    assert n_rows == m_rows