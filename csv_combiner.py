import os
import argparse
import pandas as pd
from typing import Iterable, List

def create_parser():
    parser = argparse.ArgumentParser(description='combine csv files')
    parser.add_argument('--files', type=str, nargs='+', help='The CSV file(s) to combine')
    parser.add_argument('--sep', type=str, default=',', help='The separator or delimiter used to parse the CSV files')
    return parser


def check_empty(args: argparse.Namespace) -> None:
    """
    Check to see if any of the files are empty or do not exist. Return 
    the names of all non-empty files. Note: this method will fail if one
    of the files are empty so that abnormal behavior can be detected early.

    Parameters
    ----------
    args : argparse.Namespace
        Specifies the CSV files to concatenate and the delimiter used to read 
        the files.

    Raises
    ------
    exc
        pd.errors.EmptyDataError exception or FileNotFoundError exception.

    Returns
    -------
    None
        DESCRIPTION.

    """
    file_names = args.files
    for file in file_names:
        try:
            df = pd.read_csv(file, sep=args.sep, nrows=1)
        except (pd.errors.EmptyDataError, FileNotFoundError) as exc:
            raise exc


def validate_files(args: argparse.Namespace) -> List[pd.DataFrame]:
    """
    Ensure that all files have the same column names and have
    the same number of columns. Note: this method will fail if there 
    is a column mismatch (in names or size) so that abnormal behavior
    can be detected early.

    Parameters
    ----------
    args : argparse.Namespace
        Specifies the CSV files to concatenate and the delimiter used to read 
        the files.

    Raises
    ------
    ValueError
        Error that occurs when files don't have the same column names
        or don't have the same number of columns.

    Returns
    -------
    valid_dfs : DataFrame
        Dataframes checked for column mismatch in name/size.

    """
    check_empty(args)
    file_names = args.files
    f1 = file_names[0]
    file_size = lambda file: os.path.getsize(file)/(1024**3) # in GB
    chunk_size = 10000

    # if file_size is greater than 1 GB, read the CSV file in chunks
    if file_size(f1) >= 1:
        df1 = pd.read_csv(f1, sep=args.sep, chunksize=chunk_size)
        df1 = pd.concat(df1)
    else:
        df1 = pd.read_csv(f1, sep=args.sep)
    
    n_cols = df1.shape[1]
    valid_dfs = [df1]    
    for i in range(1,len(file_names)):   
        f2 = file_names[i]

        # read in chunks if size is greater than 1 GB
        if file_size(f2) >= 1:
            df2 = pd.read_csv(f2, sep=args.sep, chunksize=chunk_size)
            df2 = pd.concat(df2)
        else:
            df2 = pd.read_csv(f2, sep=args.sep)

        m_cols = df2.shape[1]       
        if n_cols != m_cols: 
            raise ValueError('The number of columns between two files is not the same.')
        if set(df1.columns) != set(df2.columns):
            raise ValueError('The column names of two files are not the same.')

        valid_dfs.append(df2)
    return valid_dfs 


def concatenate_files(args: argparse.Namespace) -> pd.DataFrame:
    """
    File concatenation. Note: this method will fail if a file does not exist,
    is empty, or if there is a column name/size mismatch.    

    Parameters
    ----------
    args : argparse.Namespace
        Specifies the CSV files to concatenate and the delimiter used to read 
        the files.

    Returns
    -------
    output : DataFrame
        The resulting dataframe from concatenation.

    """
    # pre-processing 
    valid_dfs = validate_files(args)
    file_names = args.files
    # concatenate files
    output = pd.DataFrame()
    for i in range(len(valid_dfs)):
        df = valid_dfs[i]
        path = file_names[i]
        df['filename'] = os.path.basename(path)
        output = pd.concat([output, df], ignore_index=True) 
    return output


if __name__ == '__main__':
    parser = create_parser()
    args = parser.parse_args()
    output = concatenate_files(args).to_csv(index=False, sep=args.sep, line_terminator='\n')
    print(output)